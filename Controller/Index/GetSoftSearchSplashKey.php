<?php

declare(strict_types=1);

namespace Dividebuy\SoftSearch\Controller\Index;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\ResponseHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class GetSoftSearchSplashKey extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected ApiHelper $apiHelper;

  private ResponseHelper $responseHelper;

  public function __construct(
      Context $context,
      ApiHelper $apiHelper,
      ResponseHelper $responseHelper
  ) {
    parent::__construct($context);

    $this->apiHelper = $apiHelper;
    $this->responseHelper = $responseHelper;
  }

  /**
   * Used get instalment details and load instalmentsDetails.phtml file.
   *
   * @return ResponseInterface|ResultInterface
   *
   * @throws NoSuchEntityException
   */
  public function execute()
  {
    $totalAmount = $this->getRequest()->getparam('price');
    $totalAmount = str_replace(',', '', $totalAmount);

    $storeId = $this->responseHelper->getConfigHelper()->getStoreId();
    $splashKeyRequest = [];
    // Generate get soft search key request.
    $splashKeyRequest['retailer_base_url'] = rtrim(
        (string) $this->responseHelper->getConfigHelper()->getStore()->getBaseUrl(),
        '/'
    );
    $splashKeyRequest['order_total'] = $totalAmount;

    // Get API response and splash key
    $splashKeyResponse = $this->apiHelper->getSdkApi()->getSoftSearchKey($splashKeyRequest);
    $softSearchSplashKey = $splashKeyResponse['splashKey'] ?? '';

    // Prepare soft search popup src
    $softSearchSrc = $this->apiHelper->getSoftSearchUrl($storeId).'?splashKey='.$softSearchSplashKey;
    $softSearchHtml = '<iframe tabindex="3" style="border: 0" id="frame-dividebuy-softcredit-check" src="'.$softSearchSrc.'" width="100%" class="frame"></iframe>';

    return $this->responseHelper->sendTextResponse($softSearchHtml);
  }
}
