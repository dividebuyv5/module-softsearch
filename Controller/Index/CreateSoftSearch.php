<?php

declare(strict_types=1);

namespace Dividebuy\SoftSearch\Controller\Index;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\CartHelper;
use Dividebuy\Common\Utility\ResponseHelper;
use Dividebuy\Common\Utility\SessionHelper;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Exception;
use Magento\Checkout\Model\Type\Onepage;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;
use Throwable;

class CreateSoftSearch extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  private Onepage $onePage;
  private ApiHelper $apiHelper;
  private ResponseHelper $responseHelper;
  private SessionHelper $sessionHelper;
  private CartHelper $cartHelper;
  private StoreConfigHelper $configHelper;

  public function __construct(
      Context $context,
      Onepage $onePage,
      ApiHelper $apiHelper,
      ResponseHelper $responseHelper,
      SessionHelper $sessionHelper,
      CartHelper $cartHelper,
      StoreConfigHelper $configHelper
  ) {
    $this->onePage = $onePage;
    $this->configHelper = $configHelper;
    $this->cartHelper = $cartHelper;
    $this->sessionHelper = $sessionHelper;
    $this->apiHelper = $apiHelper;
    $this->responseHelper = $responseHelper;

    return parent::__construct($context);
  }

  /**
   * @return ResponseInterface|ResultInterface
   *
   * @throws Exception
   *
   * @throws LocalizedException
   */
  public function execute()
  {
    //checking for non dividebuy product
    $cartItems = $this->cartHelper->getSoftSearchCartItems();

    if (isset($cartItems['nodividebuy']) && $cartItems['nodividebuy'] !== 0) {
      $this->messageManager->addError(__('You still have Non-DivideBuy products in cart'));
      $response = $this->getRedirectErrorResponse($this->cartHelper->getCartUrl());
      $this->responseHelper->debugResponse($response);

      return $this->responseHelper->sendJsonResponse($response);
    }

    if ($this->sessionHelper->isLoggedIn()) {
      // Load the customer's data
      $customer = $this->sessionHelper->getCustomer();
      $firstName = $customer->getFirstname();
      $lastName = $customer->getLastname();
      $userEmail = $customer->getEmail();
    } else {
      $userEmail = $this->getRequest()->getParam('userEmail');
    }

    $userEmail = $userEmail ?? DivideBuy::DEFAULT_EMAIL;
    $this->sessionHelper->setInstallment($this->getRequest()->getParam('selected_instalment'));

    $quote = $this->sessionHelper->getQuote();
    $userShippingAddress = $quote->getShippingAddress()->getData();

    $quote->setCustomerFirstname($firstName ?? ($userShippingAddress['firstname'] ?? ''));
    $quote->setCustomerLastname($lastName ?? ($userShippingAddress['lastname'] ?? ''));
    $quote->setCustomerEmail($userEmail ?? DivideBuy::DEFAULT_EMAIL);

    // Set Sales Order Billing Address
    $quote->getBillingAddress()->addData(['email' => $userEmail]);
    $quote->getShippingAddress()->addData(['email' => $userEmail]);

    // Set Sales Order Shipping Address
    $shippingAddress = $quote->getShippingAddress();

    // Collect Rates and Set Shipping & Payment Method
    $shippingAddress->setCollectShippingRates(false)
        ->collectShippingRates()
        ->setShippingMethod($quote->getShippingAddress()->getShippingMethod()); //shipping method

    $quote->setPaymentMethod(DivideBuy::DIVIDEBUY_PAYMENT_CODE); //payment method

    // Set Sales Order Payment
    $quote->getPayment()->importData(['method' => DivideBuy::DIVIDEBUY_PAYMENT_CODE]);

    // Collect Totals & Save Quote
    $quote->collectTotals()->save();

    try {
      $this->onePage->saveOrder();

      $orderId = $this->sessionHelper->getLastOrderId();
      $order = $this->cartHelper->getOrderById($orderId);
      $splashKey = $this->configHelper->generateSplashKey($orderId);
      $redirectUrl = $this->apiHelper->getSplashRedirectUrl($this->configHelper->getStoreId(), $splashKey);

      // Update order status.
      $order->setState(Order::STATE_NEW)->setStatus(DivideBuy::DIVIDEBUY_ORDER_STATUS);
      $order->save();

      $result = $this->getRedirectResponse(true, $redirectUrl, ['customerEmail' => $userEmail]);

      $this->onePage->getQuote()->save();
      //restore cart for adding the non divide buy products
      $this->sessionHelper->restoreQuote();
      $this->cartHelper->getCartItems();

      return $this->responseHelper->sendJsonResponse($result);
    } catch (Exception $exception) {
      return $this->handleExceptionResponse($exception);
    }
  }

  public function handleExceptionResponse(Throwable $exception)
  {
    if ($this->sessionHelper->hasTemporaryCart()) {
      $this->sessionHelper->addSessionProducts();
    }
    $response = ['error' => 1, 'success' => 0, 'message' => $exception->getMessage()];
    $this->responseHelper->debugResponse($response, $exception);

    return $this->responseHelper->sendJsonResponse($response);
  }
}
